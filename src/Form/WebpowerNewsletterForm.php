<?php

namespace Drupal\webpower\Form;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\webpower\Controller\WebpowerContactController;
use Drupal\webpower\Plugin\Block\WebpowerNewsletterFormBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Implements an example form.
 */
class WebpowerNewsletterForm extends FormBase {

  /**
   * The Webpower Contact controller.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The Webpower Contact controller.
   *
   * @var \Drupal\webpower\Controller\WebpowerContactController
   */
  protected WebpowerContactController $contact;

  /**
   * The serializer interface.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The parent block.
   *
   * @var \Drupal\webpower\Plugin\Block\WebpowerNewsletterFormBlock
   */
  protected $parent_block;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): WebpowerNewsletterForm|static {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('language_manager'),
      $container->get('webpower.contact'),
      $container->get('serializer'),
      $container->get('messenger'),
    );
  }

  /**
   * WebpowerSettingsForm constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language Manager Interface.
   * @param \Drupal\webpower\Controller\WebpowerContactController $contact_controller
   *   Connection service.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer interface.
   */
  public function __construct(
    LanguageManagerInterface $language_manager,
    WebpowerContactController $contact_controller,
    SerializerInterface $serializer,
    MessengerInterface $messenger) {
    $this->languageManager = $language_manager;
    $this->contact = $contact_controller;
    $this->serializer = $serializer;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webpower_newsletter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $data = NULL, WebpowerNewsletterFormBlock $parent_block = NULL) {
    // If we have a parent (aka caller) block, store it for later use.
    if ($parent_block) {
      $this->parent_block = $parent_block;
    }

    foreach ($data['fields'] as $field) {
      $form[$field['api-field']] = [
        '#type' => $field['mapped-field'],
        '#title' => ucfirst($field['api-field']),
        '#required' => TRUE,
      ];
    }

    $form['campaign_id'] = [
      '#type' => 'hidden',
      '#value' => $data['campaign_id'],
    ];

    $form['lang'] = [
      '#type' => 'hidden',
      '#value' => $data['lang'],
    ];

    $form['groups'] = [
      '#type' => 'hidden',
      '#value' => $data['groups'],
    ];

    $form['mailing'] = [
      '#type' => 'hidden',
      '#value' => $data['mailing'],
    ];

    $configuration = $this->parent_block->getConfiguration();
    if (!empty($configuration['use_optin'])) {
      $message_optin = $this->parent_block->getConfiguration()['message_optin'];
      $form['optin'] = [
        '#type' => 'checkbox',
        '#title' => check_markup($message_optin['value'], $message_optin['format']),
        '#required' => TRUE,
      ];
    }


    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Subscribe'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \JsonException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get form values.
    $values = $form_state->getValues();

    // Build Data array.
    $data['email'] = $values['email'];
    $data['lang'] = $values['lang'] === 0 ? $this->languageManager->getCurrentLanguage()->getId() : $values['lang'];

    // Exclude Drupal related data from $values.
    $excludeKeys = [
      'campaign_id',
      'email',
      'form_build_id',
      'form_token',
      'form_id',
      'op',
      'submit',
    ];

    $data['custom'] = array_filter($values, static function ($key) use ($excludeKeys) {
      return !in_array($key, $excludeKeys, TRUE);
    }, ARRAY_FILTER_USE_KEY);

    // Transform the 'custom' array format.
    $data['custom'] = array_map(static function ($key, $value) {
      return ['field' => $key, 'value' => $value];
    }, array_keys($data['custom']), $data['custom']);

    // Submit Data to API.
    /** @var \Drupal\webpower\Model\Contact $contact */
    if ($contact = $this->contact->updateOrCreateContact($values['campaign_id'], $values['email'], $data, $values['mailing'])) {
      if (!empty($values['groups'])) {
        $this->contact->addContactToGroups((int)$values['campaign_id'], $contact->getId(), array_values($values['groups']));
      }
      if ($message = $this->getSuccessMessage()) {
        $this->messenger->addStatus($message);
      }
    }
    else {
      if ($message = $this->getFailureMessage()) {
        $this->messenger->addError($message);
      }
    }
  }

  /**
   * Returns the success message after form submission.
   *
   * @return \Drupal\Component\Render\MarkupInterface|null
   *   The message.
   */
  protected function getSuccessMessage(): ?MarkupInterface {
    if (!empty($this->parent_block) && !empty($this->parent_block->getConfiguration()['message_success']['value'])) {
      $message = $this->parent_block->getConfiguration()['message_success'];
      return check_markup($message['value'], $message['format']);
    }

    return NULL;
  }

  /**
   * Returns the failure message after form submission.
   *
   * @return \Drupal\Component\Render\MarkupInterface|null
   *   The message.
   */
  protected function getFailureMessage(): ?MarkupInterface {
    if (!empty($this->parent_block) && !empty($this->parent_block->getConfiguration()['message_failure']['value'])) {
      $message = $this->parent_block->getConfiguration()['message_failure'];
      return check_markup($message['value'], $message['format']);
    }

    return NULL;
  }

}
