<?php

namespace Drupal\webpower\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webpower\Client\BaseClient;
use Drupal\webpower\Controller\WebpowerCampaignController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Webpower module.
 */
class WebpowerSettingsForm extends ConfigFormBase {

  /**
   * The Webpower Connection service.
   *
   * @var \Drupal\webpower\Client\BaseClient
   */
  protected BaseClient $baseClient;

  /**
   * The Webpower Campaign controller.
   *
   * @var \Drupal\webpower\Controller\WebpowerCampaignController
   */
  protected WebpowerCampaignController $campaign;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): WebpowerSettingsForm|ConfigFormBase|static {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('config.factory'),
      $container->get('webpower.base_client'),
      $container->get('webpower.campaign'),
    );
  }

  /**
   * WebpowerSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\webpower\Client\BaseClient $base_client
   *   Connection service.
   * @param \Drupal\webpower\Controller\WebpowerCampaignController $campaign_controller
   *   Campaign controller.
   */
  public function __construct(ConfigFactoryInterface $config_factory, BaseClient $base_client, WebpowerCampaignController $campaign_controller) {
    parent::__construct($config_factory);
    $this->baseClient = $base_client;
    $this->campaign = $campaign_controller;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'webpower_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['webpower.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('webpower.settings');
    $connectionTest = $this->baseClient->testConnection();

    $form['api_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('REST API Settings'),
    ];

    if ($connectionTest) {
      $form['api_settings']['success_message'] = [
        '#markup' => '<div class="messages messages--status">' . $this->t('Connection with REST API established!') . '</div>',
      ];
    }

    $form['api_settings']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];

    $form['api_settings']['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('client_secret'),
      '#required' => TRUE,
    ];

    $form['api_settings']['license'] = [
      '#type' => 'textfield',
      '#title' => $this->t('License'),
      '#default_value' => $config->get('license'),
      '#required' => TRUE,
    ];

    $form['log_debug_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Logging & Debugging Settings'),
    ];

    $form['log_debug_settings']['logging_watchdog'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log Webpower API calls in Watchdog'),
      '#default_value' => $config->get('logging_watchdog'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {

    // Save Config.
    $this->config('webpower.settings')
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('license', $form_state->getValue('license'))
      ->set('logging_watchdog', $form_state->getValue('logging_watchdog'))
      ->save();

    // Test Connection.
    $test_result = $this->baseClient->testConnection();
    if (!$test_result) {
      $form_state->setErrorByName('license', $this->t('Could not connect to API.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('webpower.settings')
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('license', $form_state->getValue('license'))
      ->set('logging_watchdog', $form_state->getValue('logging_watchdog'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
