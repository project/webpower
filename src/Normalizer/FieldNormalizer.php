<?php

namespace Drupal\webpower\Normalizer;

use Drupal\webpower\Model\Field;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * {@inheritdoc}
 */
class FieldNormalizer implements DenormalizerInterface {

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []): Field {
    $field = new Field();

    $field->setName($data['name']);
    $field->setDescription($data['description'] ?? []);
    $field->setType($data['type'] ?? '');
    $field->setLength($data['length'] ?? 0);
    $field->setDefault($data['default'] ?? NULL);
    $field->setRequired($data['required'] ?? FALSE);

    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDenormalization($data, $type, $format = NULL): bool {
    return $type === Field::class;
  }

}
