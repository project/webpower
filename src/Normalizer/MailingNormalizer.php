<?php

namespace Drupal\webpower\Normalizer;

use Drupal\webpower\Model\Mailing;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * {@inheritdoc}
 */
class MailingNormalizer implements DenormalizerInterface {

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []): Mailing {
    $mailing = new Mailing();

    $mailing->setId($data['id'] ?? NULL);
    $mailing->setName($data['name'] ?? NULL);
    $mailing->setLang($data['lang'] ?? NULL);
    $mailing->setSubject($data['subject'] ?? NULL);
    $mailing->setPreheader($data['preheader'] ?? NULL);
    $mailing->setFromName($data['from_name'] ?? NULL);
    $mailing->setForwardId($data['forward_id'] ?? NULL);
    $mailing->setReplyId($data['reply_id'] ?? NULL);
    $mailing->setPlaintextMsg($data['plaintext_msg'] ?? NULL);
    $mailing->setHtmlMsg($data['html_msg'] ?? NULL);
    $mailing->setOptinConfirmCandidate($data['optin_confirm_candidate'] ?? FALSE);
    $mailing->setEmbedPics($data['embed_pics'] ?? FALSE);
    $mailing->setEmbedTrackpixel($data['embed_trackpixel'] ?? FALSE);
    $mailing->setTransferEncoding($data['transfer_encoding'] ?? NULL);
    $mailing->setTransferCharset($data['transfer_charset'] ?? NULL);

    if (isset($data['last_def_sent_date']) && $data['last_def_sent_date'] !== NULL) {
      $lastDefSentDate = new \DateTime($data['last_def_sent_date']);
      $mailing->setLastDefSentDate($lastDefSentDate);
    }
    return $mailing;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDenormalization($data, $type, $format = NULL): bool {
    return $type === Mailing::class;
  }

}
