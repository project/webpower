<?php

namespace Drupal\webpower\Normalizer;

use Drupal\webpower\Model\Contact;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * {@inheritdoc}
 */
class ContactNormalizer implements DenormalizerInterface {

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []): Contact {
    $contact = new Contact();

    $contact->setId($data['id'] ?? NULL);
    $contact->setCreateDate($data['create_date'] ?? NULL);
    $contact->setEmail($data['email'] ?? NULL);
    $contact->setMobileNr($data['mobile_nr'] ?? NULL);
    $contact->setLang($data['lang'] ?? NULL);

    if (isset($data['custom']) && is_array($data['custom'])) {
      $contact->setCustom($data['custom']);
    }

    return $contact;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDenormalization($data, $type, $format = NULL): bool {
    return $type === Contact::class;
  }

}
