<?php

namespace Drupal\webpower\Normalizer;

use Drupal\webpower\Model\Group;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * {@inheritdoc}
 */
class GroupNormalizer implements DenormalizerInterface {

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []): Group {
    $group = new Group();

    $group->setId($data['id'] ?? NULL);
    $group->setName($data['name'] ?? '');
    $group->setIsTest($data['is_test'] ?? FALSE);
    $group->setIsActive($data['is_active'] ?? FALSE);
    $group->setIsSystem($data['is_system'] ?? FALSE);
    $group->setRemarks($data['remarks'] ?? '');

    return $group;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDenormalization($data, $type, $format = NULL): bool {
    return $type === Group::class;
  }

}
