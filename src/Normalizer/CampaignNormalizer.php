<?php

namespace Drupal\webpower\Normalizer;

use Drupal\webpower\Model\Campaign;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * {@inheritdoc}
 */
class CampaignNormalizer implements DenormalizerInterface {

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []): Campaign {
    $campaign = new Campaign();

    $campaign->setId($data['id']);
    $campaign->setBrandId($data['brand_id']);
    $campaign->setName($data['name']);
    $campaign->setLang($data['lang']);
    $campaign->setOverall($data['overall']);
    $campaign->setDeletable($data['deletable']);
    $campaign->setTemplateOnly($data['template_only']);
    $campaign->setFromName($data['from_name']);
    $campaign->setForwardId($data['forward_id'] ?? NULL);
    $campaign->setReplyId($data['reply_id'] ?? NULL);
    $campaign->setSmsFromName($data['sms_from_name'] ?? NULL);
    $campaign->setConversionPoints($data['conversion_points']);
    $campaign->setHasSoapApi($data['has_soap_api']);
    $campaign->setBlacklists($data['blacklists']);
    $campaign->setLocalDomain($data['local_domain']);

    return $campaign;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDenormalization($data, $type, $format = NULL): bool {
    return $type === Campaign::class;
  }

}
