<?php

namespace Drupal\webpower\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\webpower\Client\ContactService;
use Drupal\webpower\Model\Contact;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Used to manage campaigns.
 */
class WebpowerContactController extends ControllerBase {

  /**
   * The campaign service.
   *
   * @var \Drupal\webpower\Client\ContactService
   */
  protected $contactService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('webpower.contact_service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ContactService $contactService) {
    $this->contactService = $contactService;
  }

  /**
   * Get contact.
   *
   * @param int $campaignId
   *   The Campaign ID of the contact.
   * @param int $id
   *   The contact ID to locate.
   *
   * @return \Drupal\webpower\Model\Contact
   *   Returns contact object.
   *
   * @throws \JsonException
   */
  public function getContact(int $campaignId, int $id): Contact {
    return $this->contactService->fetchContact($campaignId, $id);
  }

  /**
   * Return Contacts, useable for a select list.
   *
   * @param int $campaignId
   *   The Campaign ID of the contact.
   * @param string $email
   *   The Email address to search on.
   *
   * @return \Drupal\webpower\Model\Contact|bool
   *   Returns array of contacts or false if no contacts.
   *
   * @throws \JsonException
   */
  public function getContactByEmail(int $campaignId, string $email): Contact|bool {
    $searchString = json_encode(['email' => $email]);
    $rawContacts = $this->contactService->fetchContacts($campaignId, 1, 10, $searchString);

    if (!empty($rawContacts)) {
      return reset($rawContacts);
    }

    return FALSE;
  }

  /**
   * Checks if a contact exists and updates / creates it.
   *
   * If Mailing string is passed to the function the new contact will be subbed.
   *
   * @param int $campaignId
   *   The Campaign ID of the contact.
   * @param string $email
   *   The Email address to search on.
   * @param array $data
   *   Data to submit.
   * @param string|null $mailing
   *   Mailing name.
   *
   * @return \Drupal\webpower\Model\Contact|bool
   *   Returns the created / updated contact.
   *
   * @throws \JsonException
   */
  public function updateOrCreateContact(int $campaignId, string $email, array $data, string $mailing = NULL): Contact|bool {

    // Check if contact exists.
    $contact = $this->getContactByEmail($campaignId, $email);

    if ($contact) {
      $this->contactService->updateContact($campaignId, $contact->getId(), $data);
    }
    elseif ($mailing) {
      $this->contactService->createContact($campaignId, $data);
    }
    else {
      $subscribeData['mailingName'] = $mailing;
      $subscribeData['contact'] = $data;
      $this->subscribe($campaignId, $subscribeData);
    }

    // Get recently updated / created contact & Return.
    $updatedContact = $this->getContactByEmail($campaignId, $email);

    if ($updatedContact) {
      return $updatedContact;
    }

    return FALSE;
  }

  /**
   * Adds an existing contact to one or more groups of a campaign.
   *
   * @param int $campaignId
   *   The Campaign ID of the contact.
   * @param int $id
   *   The Contact ID.
   * @param array $data
   *   Data to submit.
   *
   * @throws \JsonException
   */
  public function addContactToGroups(int $campaignId, int $id, array $data): void {
    $this->contactService->addToGroups($campaignId, $id, $data);
  }

  /**
   * Adds an existing contact to one or more groups of a campaign.
   *
   * @param int $campaignId
   *   The Campaign ID of the contact.
   * @param array $data
   *   Data to submit.
   *
   * @throws \JsonException
   */
  public function subscribe(int $campaignId, array $data): void {
    $this->contactService->subscribeToMailing($campaignId, $data);
  }

}
