<?php

namespace Drupal\webpower\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\webpower\Client\CampaignService;
use Drupal\webpower\Model\Campaign;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Used to manage campaigns.
 */
class WebpowerCampaignController extends ControllerBase {

  /**
   * The campaign service.
   *
   * @var \Drupal\webpower\Client\CampaignService
   */
  protected $campaignService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('webpower.campaign_service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(CampaignService $campaignService) {
    $this->campaignService = $campaignService;
  }

  /**
   * Return Campaigns useable for select list.
   *
   * @return array|bool
   *   Array of Campaign objects.
   *
   * @throws \JsonException
   */
  public function getCampaigns(): array|bool {
    $rawCampaigns = $this->campaignService->fetchCampaigns();

    if ($rawCampaigns) {
      return $this->transformCampaigns($rawCampaigns);
    }

    return FALSE;
  }

  /**
   * Return single Campaign object.
   *
   * @throws \JsonException
   */
  public function getCampaign(int $id): Campaign {
    return $this->campaignService->fetchCampaign($id);
  }

  /**
   * Return single Campaign fields.
   *
   * @return arrayField
   *   List of Field objects.
   *
   * @throws \JsonException
   */
  public function getCampaignFields(int $campaignId): array {
    return $this->campaignService->fetchCampaignFields($campaignId);
  }

  /**
   * Return an array of the languages assigned to the Campaign.
   *
   * @return array
   *   Returns an array of language codes.
   *
   * @throws \JsonException
   */
  public function getCampaignLanguages(int $campaignId): array {
    $campaign = $this->getCampaign($campaignId);

    $languages = [];
    foreach ($campaign->getLang() as $language) {
      $languages[$language] = $language;
    }

    return $languages;
  }

  /**
   * Transfor raw campaign list from json to select list data.
   *
   * Params $campaigns
   *  Json Campaign list.
   *
   * @return arrayintstring
   *   Returns array of Campaigns useable in select.
   */
  private function transformCampaigns(array $campaigns): array {
    $finalArray = [];
    /** @var \Drupal\webpower\Model\Campaign $campaign */
    foreach ($campaigns as $campaign) {
      $finalArray[$campaign->getId()] = $campaign->getName();
    }
    return $finalArray;
  }

}
