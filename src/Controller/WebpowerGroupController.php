<?php

namespace Drupal\webpower\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\webpower\Client\GroupService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Used to manage campaigns.
 */
class WebpowerGroupController extends ControllerBase {

  /**
   * The campaign service.
   *
   * @var \Drupal\webpower\Client\GroupService
   */
  protected $groupService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('webpower.group_service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(GroupService $groupService) {
    $this->groupService = $groupService;
  }

  /**
   * Return Groups useable for a select list.
   */
  public function getGroups(int $campaignId): array {
    $groups = $this->groupService->fetchGroups($campaignId);
    return $this->transformGroups($groups);
  }

  /**
   * Transform raw group list from json to select list data.
   *
   * Params $campaigns
   *  Json Campaign list.
   */
  private function transformGroups(array $groups): array {
    $finalArray = [];
    foreach ($groups as $group) {
      $finalArray[$group->getId()] = $group->getName();
    }
    return $finalArray;
  }

}
