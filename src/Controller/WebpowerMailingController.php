<?php

namespace Drupal\webpower\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\webpower\Client\MailingService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Used to manage campaigns.
 */
class WebpowerMailingController extends ControllerBase {

  /**
   * The campaign service.
   *
   * @var \Drupal\webpower\Client\MailingService
   */
  protected $mailingService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('webpower.mailing_service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(MailingService $mailingService) {
    $this->mailingService = $mailingService;
  }

  /**
   * Return Groups useable for a select list.
   */
  public function getMailings(int $campaignId): array {
    $mailings = $this->mailingService->fetchMailings($campaignId);
    return $this->transformMailings($mailings);
  }

  /**
   * Transform raw group list from json to select list data.
   *
   * Params $campaigns
   *  Json Campaign list.
   *
   * @return array
   *   Array of mailings usable in select.
   */
  private function transformMailings(array $mailings): array {
    $finalArray = [];
    foreach ($mailings as $mailing) {
      $mailingName = $mailing->getName();
      $finalArray[$mailingName] = $mailingName;
    }
    return $finalArray;
  }

}
