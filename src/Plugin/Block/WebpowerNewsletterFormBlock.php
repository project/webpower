<?php

namespace Drupal\webpower\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\webpower\Controller\WebpowerCampaignController;
use Drupal\webpower\Controller\WebpowerGroupController;
use Drupal\webpower\Controller\WebpowerMailingController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'CampaignSelectBlock' block.
 *
 * @Block(
 *  id = "webpower_newsletter_form_block",
 *  admin_label = @Translation("Webpower - Newsletter Form Block"),
 * )
 */
class WebpowerNewsletterFormBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The campaign controller.
   *
   * @var \Drupal\webpower\Controller\WebpowerCampaignController
   */
  protected $campaign;

  /**
   * The group controller.
   *
   * @var \Drupal\webpower\Controller\WebpowerGroupController
   */
  protected $group;

  /**
   * The group controller.
   *
   * @var \Drupal\webpower\Controller\WebpowerMailingController
   */
  protected $mailing;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * WebpowerFormBlock constructor.
   *
   * @param array $configuration
   *   Config.
   * @param string $plugin_id
   *   Plugin.
   * @param mixed $plugin_definition
   *   Plugin def.
   * @param \Drupal\webpower\Controller\WebpowerCampaignController $campaign
   *   Campaign controller.
   * @param \Drupal\webpower\Controller\WebpowerGroupController $group
   *   Group controller.
   * @param \Drupal\webpower\Controller\WebpowerMailingController $mailing
   *   Mailing controller.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    WebpowerCampaignController $campaign,
    WebpowerGroupController $group,
    WebpowerMailingController $mailing,
    FormBuilderInterface $form_builder
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->campaign = $campaign;
    $this->group = $group;
    $this->mailing = $mailing;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('webpower.campaign'),
      $container->get('webpower.group'),
      $container->get('webpower.mailing'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    // This method receives a sub form state instead of the full form state.
    // There is an ongoing discussion around this which could result in the
    // passed form state going back to a full form state. In order to prevent
    // future breakage because of a core update we'll just check which type of
    // FormStateInterface we've been passed and act accordingly.
    // @See https://www.drupal.org/node/2798261
    $complete_form_state = $form_state instanceof SubformStateInterface ? $form_state->getCompleteFormState() : $form_state;

    $form = parent::blockForm($form, $form_state);
    $campaigns = $this->campaign->getCampaigns();

    if ($campaigns) {
      $config = $this->getConfiguration();
      $campaign_id = $config['campaign_id'] ?? array_key_first($campaigns);

      // Campaign Select.
      $form['campaign_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Select campaign'),
        '#options' => $campaigns,
        '#default_value' => $campaign_id,
        '#required' => TRUE,
        '#ajax' => [
          'callback' => [$this, 'updateCampaignFields'],
          'wrapper' => 'campaign-fields-wrapper',
          'event' => 'change',
        ],
      ];

      // Start Field Mapping.
      $form['campaign_fields_wrapper'] = [
        '#type' => 'container',
        '#attributes' => ['id' => 'campaign-fields-wrapper'],
      ];

      // Define Field Types.
      $field_types = [
        'textfield' => $this->t('Text'),
        'textarea' => $this->t('Textarea'),
        'number' => $this->t('Number'),
        'date' => $this->t('Date'),
        'datetime' => $this->t('Date & Time'),
        'email' => $this->t('E-Mail'),
        'tel' => $this->t('Telephone'),
      ];

      if (!empty($complete_form_state->getValue(['settings', 'campaign_id']))) {
        $campaign_id = $complete_form_state->getValue(['settings', 'campaign_id']);
      }

      // Get Associated Campaign fields.
      $fields = $this->campaign->getCampaignFields($campaign_id);

      $languages = array_merge(
        [0 => '- Current Site Language -'],
        $this->campaign->getCampaignLanguages($campaign_id)
      );

      $form['campaign_fields_wrapper']['lang'] = [
        '#type' => 'select',
        '#title' => 'Campaign Language',
        '#options' => $languages,
        '#default_value' => $config['lang'] ?? '',
      ];

      $form['campaign_fields_wrapper']['groups'] = [
        '#type' => 'select',
        '#title' => 'Add subscriber to group(s)',
        '#options' => $this->group->getGroups($campaign_id),
        '#multiple' => TRUE,
        '#default_value' => $config['groups'] ?? '',
      ];

      $form['campaign_fields_wrapper']['mailing'] = [
        '#type' => 'select',
        '#title' => 'Subscribe subscriber to mailing',
        '#empty_option' => '- Select mailing -',
        '#options' => $this->mailing->getMailings($campaign_id),
        '#default_value' => $config['mailing'] ?? '',
        '#required' => FALSE,
      ];

      $form['campaign_fields_wrapper']['campaign_fields'] = [
        '#type' => 'table',
        '#title' => 'Field mapping',
        '#header' => [
          'name' => $this->t('Field name'),
          'type' => $this->t('Field type'),
          'hidden' => NULL,
          'field_type' => $this->t('Mapped Type'),
        ],
        '#empty' => $this->t('No fields found.'),
      ];

      // Create table rows.
      /**
       * @var int $key
       * @var \Drupal\webpower\Model\Field $field
       */
      foreach ($fields as $key => $field) {
        $form['campaign_fields_wrapper']['campaign_fields'][$key]['name'] = [
          '#markup' => "<strong>" . $field->getName() . "</strong>",
        ];

        $form['campaign_fields_wrapper']['campaign_fields'][$key]['type'] = [
          '#markup' => $field->getType(),
        ];

        $form['campaign_fields_wrapper']['campaign_fields'][$key]["api-field-$key"] = [
          '#type' => 'hidden',
          '#value' => $field->getName(),
        ];

        $form['campaign_fields_wrapper']['campaign_fields'][$key]["mapped-field-$key"] = [
          '#type' => 'select',
          '#options' => $field_types,
          '#default_value' => $config['campaign_fields'][$key]['mapped-field'] ?? 0,
          '#required' => TRUE,
        ];
      }
    }
    else {
      $form['not_found'] = [
        '#markup' => '<div class="messages messages--error">' . $this->t('No campaings found, check your API settings.') . '</div>',
      ];
    }

    $form['webpower'] = [
      '#type' => 'vertical_tabs',
    ];

    // Opt-in.
    $form['optin'] = [
      '#type' => 'details',
      '#title' => $this->t('Opt-in'),
      '#group' => 'webpower',
      '#open' => TRUE,
    ];
    $form['optin']['use_optin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable opt-in checkbox?'),
      '#default_value' => $config['use_optin'] ?? 1,
    ];
    $form['optin']['message_optin'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Opt-in message'),
      '#default_value' => $config['message_optin']['value'] ?? '',
      '#format' => $config['message_optin']['format'] ?? '',
      '#states' => [
        'visible' => [
          ':input[name="settings[optin][use_optin]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Messages.
    $form['messages'] = [
      '#type' => 'details',
      '#title' => $this->t('Messages'),
      '#group' => 'webpower',
    ];
    $form['messages']['message_success'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Success message'),
      '#default_value' => $config['message_success']['value'] ?? '',
      '#format' => $config['message_success']['format'] ?? '',
      '#required' => FALSE,
    ];
    $form['messages']['message_failure'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Failure message'),
      '#default_value' => $config['message_failure']['value'] ?? '',
      '#format' => $config['message_failure']['format'] ?? '',
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * Ajax callback to update the campaign fields based on the selected campaign.
   */
  public function updateCampaignFields(array $element, FormStateInterface $form_state): array {
    return $element['settings']['campaign_fields_wrapper'];
  }

 /**
  * {@inheritdoc}
  */
  public function blockValidate($form, FormStateInterface $form_state) {
    $triggered_element = $form_state->getTriggeringElement();
    if ($triggered_element['#name'] === 'settings[campaign_id]') {
      $form_state->clearErrors();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {

    // Save Data.
    $this->setConfigurationValue('campaign_id', $form_state->getValue('campaign_id'));
    $this->setConfigurationValue('lang', $form_state->getValue('campaign_fields_wrapper')['lang'] ?? NULL);
    $this->setConfigurationValue('groups', $form_state->getValue('campaign_fields_wrapper')['groups'] ?? NULL);
    $this->setConfigurationValue('mailing', $form_state->getValue('campaign_fields_wrapper')['mailing'] ?? NULL);
    $this->setConfigurationValue('use_optin', $form_state->getValue('optin')['use_optin']);
    $this->setConfigurationValue('message_optin', $form_state->getValue('optin')['message_optin']);
    $this->setConfigurationValue('message_success', $form_state->getValue('messages')['message_success']);
    $this->setConfigurationValue('message_failure', $form_state->getValue('messages')['message_failure']);

    // Find mapped fields.
    $campaignFields = $form_state->getValue('campaign_fields_wrapper')['campaign_fields'] ?? [];
    $fieldMappings = [];
    foreach ($campaignFields as $key => $field) {
      if (isset($field["api-field-$key"], $field["mapped-field-$key"])) {
        $fieldMappings[$key]['api-field'] = $field["api-field-$key"];
        $fieldMappings[$key]['mapped-field'] = $field["mapped-field-$key"];
      }
    }

    // Save the field mappings to the block's configuration.
    $this->setConfigurationValue('campaign_fields', $fieldMappings);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {

    // Compile data.
    $config = $this->getConfiguration();

    if ($config['campaign_id']) {
      $data = [
        'campaign_id' => $config['campaign_id'],
        'lang' => $config['lang'],
        'groups' => $config['groups'],
        'mailing' => $config['mailing'],
        'fields' => $config['campaign_fields'],
      ];

      // Render form with data.
      return $this->formBuilder->getForm('Drupal\webpower\Form\WebpowerNewsletterForm', $data, $this);
    }

    return [];
  }

}
