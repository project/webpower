<?php

namespace Drupal\webpower\Client;

use Drupal\webpower\Model\Campaign;
use Drupal\webpower\Model\Field;

/**
 * Handles Campaigns from the Webpower REST API.
 */
class CampaignService extends BaseClient {

  /**
   * Fetches the campaigns.
   *
   * @param int $page
   *   Which page to retrieve.
   * @param int $pagelength
   *   The amount of records per page.
   *
   * @return array|bool
   *   Returns list of Campaigns.
   *
   * @throws \JsonException
   */
  public function fetchCampaigns(int $page = 1, int $pagelength = 1000):array|bool {
    $data = $this->get("/admin/api/index.php/rest/campaign?page=$page&pagelength=$pagelength");

    if ($data) {
      return array_map(function ($campaign) {
        return $this->serializer->deserialize(json_encode($campaign, JSON_THROW_ON_ERROR), Campaign::class, 'json');
      }, $data['result']);
    }

    return FALSE;
  }

  /**
   * Fetches a single campaign based on its ID.
   *
   * @param int $id
   *   The campaign ID.
   *
   * @return \Drupal\webpower\Model\Campaign
   *   Returns requested Campaign object.
   *
   * @throws \JsonException
   */
  public function fetchCampaign(int $id): Campaign {
    $data = $this->get("/admin/api/index.php/rest/campaign/$id");
    return $this->serializer->deserialize(json_encode($data, JSON_THROW_ON_ERROR), Campaign::class, 'json');
  }

  /**
   * Fetches all the fields associated with a campaign.
   *
   * @param int $campaignId
   *   The campaign ID.
   *
   * @return array
   *   An array of campaign field objects..
   */
  public function fetchCampaignFields(int $campaignId): array {
    $data = $this->get("/admin/api/index.php/rest/$campaignId/field");
    return array_map(function ($field) {
      return $this->serializer->deserialize(json_encode($field, JSON_THROW_ON_ERROR), Field::class, 'json');
    }, $data['result']);
  }

}
