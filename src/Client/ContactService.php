<?php

namespace Drupal\webpower\Client;

use Drupal\webpower\Model\Contact;

/**
 * Handles Contacts from the Webpower REST API.
 */
class ContactService extends BaseClient {

  /**
   * Fetches campaign contacts.
   *
   * @param int $campaignId
   *   Campaign ID of the contacts to retrieve.
   * @param int $page
   *   Which page to retrieve.
   * @param int $pagelength
   *   The amount of records per page.
   * @param string $match
   *   Match contacts on the given field values (json object with field as key).
   * @param int $groupID
   *   Contacts that are in the given group.
   * @param string $fromDate
   *   When groupId is given, return contacts in the group on / after this date.
   *
   * @return array|bool
   *   An array of campaigns if successful, FALSE otherwise.
   */
  public function fetchContacts(
    int $campaignId,
    int $page = 1,
    int $pagelength = 10,
    string $match = NULL,
    int $groupID = NULL,
    string $fromDate = '0000-01-01 00:00:00',
  ): bool|array {

    // Do not include groupID (& fromDate) if it is empty.
    if ($groupID !== NULL) {
      $string = "/admin/api/index.php/rest/$campaignId/contact?page=$page&pagelength=$pagelength&match=" . urlencode($match) . "&groupId=$groupID&fromDate=" . urlencode($fromDate);
    }
    else {
      $string = "/admin/api/index.php/rest/$campaignId/contact?page=$page&pagelength=$pagelength&match=" . urlencode($match);
    }

    if ($data = $this->get($string)) {
      return array_map(function ($contact) {
        return $this->serializer->deserialize(json_encode($contact, JSON_THROW_ON_ERROR), Contact::class, 'json');
      }, $data['result']);
    }

    return FALSE;
  }

  /**
   * Fetches specific contact.
   *
   * @param int $campaignId
   *   Campaing id to send the contact too.
   * @param int $id
   *   Contact id to fetch.
   *
   * @return \Drupal\webpower\Model\Contact
   *   Returns contact object.
   */
  public function fetchContact(int $campaignId, int $id): Contact {
    $data = $this->get("/admin/api/index.php/rest/$campaignId/contact/$id");
    return $this->serializer->deserialize(json_encode($data, JSON_THROW_ON_ERROR), Contact::class, 'json');
  }

  /**
   * Create new contact.
   *
   * @param int $campaignId
   *   Campaing id to send the contact too.
   * @param array $data
   *   Contact data to submit, converted to JSON on POST.
   *
   * @throws \JsonException
   */
  public function createContact(int $campaignId, array $data): void {
    $this->post("/admin/api/index.php/rest/$campaignId/contact", $data);
  }

  /**
   * Update an existing contact.
   *
   * @param int $campaignId
   *   Campaing id to send the contact too.
   * @param int $id
   *   Contact ID.
   * @param array $data
   *   Contact data to submit, converted to JSON on POST.
   *
   * @throws \JsonException
   */
  public function updateContact(int $campaignId, int $id, array $data): void {
    $this->post("/admin/api/index.php/rest/$campaignId/contact/$id", $data, 'PUT');
  }

  /**
   * Fetches the campaigns.
   *
   * @param int $campaignId
   *   Campaing id to send the contact too.
   * @param array $data
   *   Contact data to submit, converted to JSON on POST.
   */
  public function subscribeToMailing(int $campaignId, array $data): void {
    $this->post("/admin/api/index.php/rest/$campaignId/contact/subscribe", $data);
  }

  /**
   * Fetches the campaigns.
   *
   * @param int $campaignId
   *   Campaing id to send the contact too.
   * @param int $id
   *   Contact id to add to a group.
   * @param array $data
   *   Array of Group IDs the contact needs to be added to.
   *
   * @throws \JsonException
   */
  public function addToGroups(int $campaignId, int $id, array $data): void {
    $this->post("/admin/api/index.php/rest/$campaignId/contact/$id/group", $data);
  }

}
