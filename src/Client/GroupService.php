<?php

namespace Drupal\webpower\Client;

use Drupal\webpower\Model\Group;

/**
 * Handles Groups from the Webpower REST API.
 */
class GroupService extends BaseClient {

  /**
   * Fetches a campaign's groups.
   *
   * @param int $campaignId
   *   The campaign's ID.
   * @param int $page
   *   Which page to retrieve.
   * @param int $pagelength
   *   The amount of records per page.
   *
   * @return array
   *   An array of campaigns if successful, FALSE otherwise.
   */
  public function fetchGroups(int $campaignId, int $page = 1, int $pagelength = 1000): array {
    $data = $this->get("/admin/api/index.php/rest/$campaignId/group?page=$page&pagelength=$pagelength");
    return array_map(function ($group) {
      return $this->serializer->deserialize(json_encode($group, JSON_THROW_ON_ERROR), Group::class, 'json');
    }, $data['result']);
  }

  /**
   * Fetches a single campaign based on its ID.
   *
   * @param int $campaignId
   *   The campaign ID.
   * @param int $id
   *   The group ID.
   *
   * @return \Drupal\webpower\Model\Group
   *   An array of campaign details if successful, FALSE otherwise.
   *
   * @throws \JsonException
   */
  public function fetchGroup(int $campaignId, int $id): Group {
    $data = $this->get("/admin/api/index.php/rest/$campaignId/group/$id");
    return $this->serializer->deserialize(json_encode($data, JSON_THROW_ON_ERROR), Group::class, 'json');
  }

}
