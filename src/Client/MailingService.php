<?php

namespace Drupal\webpower\Client;

use Drupal\webpower\Model\Mailing;

/**
 * Handles Contacts from the Webpower REST API.
 */
class MailingService extends BaseClient {

  /**
   * Fetches campaign contacts.
   *
   * @param int $campaignId
   *   Campaign ID of the contacts to retrieve.
   * @param int $page
   *   Which page to retrieve.
   * @param int $pagelength
   *   The amount of records per page.
   *
   * @return array
   *   An array of mailings.
   *
   * @throws \JsonException
   */
  public function fetchMailings(int $campaignId, int $page = 1, int $pagelength = 1000): array {
    $data = $this->get("/admin/api/index.php/rest/$campaignId/mailing?page=$page&pagelength=$pagelength");
    return array_map(function ($mailing) {
      return $this->serializer->deserialize(json_encode($mailing, JSON_THROW_ON_ERROR), Mailing::class, 'json');
    }, $data['result']);
  }

  /**
   * Fetches a single campaign based on its ID.
   *
   * @param int $campaignId
   *   The campaign ID.
   * @param int $id
   *   The mailing ID.
   *
   * @return \Drupal\webpower\Model\Mailing
   *   Mailing object.
   *
   * @throws \JsonException
   */
  public function fetchMailing(int $campaignId, int $id): Mailing {
    $data = $this->get("/admin/api/index.php/rest/$campaignId/mailing/$id");
    return $this->serializer->deserialize(json_encode($data, JSON_THROW_ON_ERROR), Mailing::class, 'json');
  }

}
