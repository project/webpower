<?php

namespace Drupal\webpower\Client;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Handles making connection with the Webpower REST API.
 */
class BaseClient {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerFactory;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The serializer interface.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * Constructs a new ConnectionService object.
   */
  public function __construct(CacheBackendInterface $cache_backend, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, ClientInterface $http_client, SerializerInterface $serializer) {
    $this->cache = $cache_backend;
    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
    $this->httpClient = $http_client;
    $this->serializer = $serializer;
  }

  /**
   * Get an access token.
   *
   * @return string|bool
   *   The access token, or NULL if it cannot be fetched.
   */
  public function getAccessToken(): string|bool {

    // Check for cached token.
    $cache = $this->cache->get('webpower_access_token');
    if ($cache && $cache->expire > time()) {
      return $cache->data;
    }

    try {
      $config = $this->configFactory->get('webpower.settings');

      $client_id = $config->get('client_id');
      $client_secret = $config->get('client_secret');
      $license_url = $config->get('license') . '.webpower.eu';
      $api_url = "https://$license_url/admin/oauth2/token.php";

      $response = $this->httpClient->request('POST', $api_url, [
        'form_params' => [
          'grant_type' => 'client_credentials',
          'client_id' => $client_id,
          'client_secret' => $client_secret,
          'scope' => 'rest',
        ],
      ]);

      $data = json_decode($response->getBody(), TRUE, 512, JSON_THROW_ON_ERROR);

      if ($config->get('logging_watchdog')) {
        $this->loggerFactory->get('webpower')->notice('[Webpower API] Get Access Token: @response', ['@response' => json_encode($data, JSON_PRETTY_PRINT)]);
      }

      if (isset($data['access_token'])) {
        // Cache the token until it expires.
        $this->cache->set('webpower_access_token', $data['access_token'], time() + $data['expires_in']);
        return $data['access_token'];
      }
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('webpower')->error('[Webpower API] Get Access Token failed: @error', ['@error' => $e->getMessage()]);
    }

    return FALSE;
  }

  /**
   * Tests the API connection.
   *
   * @return bool
   *   Returns TRUE if successful, FALSE otherwise.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \JsonException
   */
  public function testConnection(): bool {
    $config = $this->configFactory->get('webpower.settings');
    $license_url = $config->get('license') . '.webpower.eu';

    $accessToken = $this->getAccessToken();

    if ($accessToken && !empty($config->get('license'))) {
      try {
        $response = $this->httpClient->request('GET', "https://$license_url/admin/api/index.php/rest/campaign", [
          'headers' => [
            'Authorization' => "Bearer $accessToken",
          ],
        ]);

        if ($response->getStatusCode() === 200) {
          return TRUE;
        }
      }
      catch (\Exception $e) {
        $this->loggerFactory->get('webpower')->error('[Webpower API] Test connection failed: @error', ['@error' => $e->getMessage()]);
        return FALSE;
      }
    }

    return FALSE;
  }

  /**
   * Makes a GET API call.
   *
   * @param string $url
   *   The API URL.
   *
   * @return array|bool
   *   An array of data, FALSE otherwise.
   */
  public function get(string $url): array|bool {
    $accessToken = $this->getAccessToken();

    if ($accessToken) {
      try {
        $config = $this->configFactory->get('webpower.settings');
        $license_url = $config->get('license') . '.webpower.eu';

        $response = $this->httpClient->request('GET', "https://$license_url" . $url, [
          'headers' => [
            'Authorization' => "Bearer $accessToken",
          ],
        ]);

        $data = json_decode($response->getBody(), TRUE, 512, JSON_THROW_ON_ERROR);

        if ($config->get('logging_watchdog')) {
          $this->loggerFactory->get('webpower')->notice('[Webpower API] GET: @url | @response',
            [
              '@url' => "https://$license_url" . $url,
              '@response' => json_encode($data, JSON_PRETTY_PRINT),
            ]);
        }

        if ($response->getStatusCode() === 200) {
          return $data;
        }
      }
      catch (\Exception $e) {
        $this->loggerFactory->get('webpower')->error(
          '[Webpower API] GET request failed: @url | @error',
          [
            '@url' => "https://$license_url" . $url,
            '@error' => '[' . $e->getLine() . ']: ' . $e->getMessage(),
          ]
        );
      }
    }

    return FALSE;
  }

  /**
   * Makes a POST API call.
   *
   * @param string $url
   *   The API URL.
   * @param array $data
   *   The data to submit to the API.
   * @param string $method
   *   The method with which to request the API.
   *
   * @return bool
   *   TRUE if request succeeded, FALSE otherwise.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function post(string $url, array $data, string $method = 'POST') {
    $accessToken = $this->getAccessToken();

    if ($accessToken) {
      try {
        $config = $this->configFactory->get('webpower.settings');
        $license_url = $config->get('license') . '.webpower.eu';

        $response = $this->httpClient->request($method, "https://$license_url" . $url, [
          'headers' => [
            'Authorization' => "Bearer $accessToken",
            'Content-Type' => 'application/JSON',
          ],
          'body' => json_encode($data, JSON_THROW_ON_ERROR),
        ]);

        if ($config->get('logging_watchdog')) {
          $responseData = (string) $response->getBody();
          $this->loggerFactory->get('webpower')->notice('[Webpower API] @method: @url | @response',
            [
              '@method' => $method,
              '@url' => "https://$license_url" . $url,
              '@response' => $responseData,
            ]);
        }

        return $response->getStatusCode() === 200;
      }
      catch (\Exception $e) {
        $this->loggerFactory->get('webpower')->error(
          '[Webpower API] @method request failed: @url | @error',
            [
              '@method' => $method,
              '@url' => "https://$license_url" . $url,
              '@error' => '[' . $e->getLine() . ']: ' . $e->getMessage(),
            ]
        );
      }
    }

    return FALSE;
  }

}
