<?php

namespace Drupal\webpower\Model;

// phpcs:ignore
use Symfony\Component\Validator\Constraints\Type;

/**
 * The Contact model definition.
 */
class Contact {

  /**
   * Contact's ID.
   *
   * @var int
   * @Type("int")
   */
  private int $id;

  /**
   * Creation date of the contact.
   *
   * @var string
   * @Type("string")
   */
  private string $createDate;

  /**
   * Email address of the contact.
   *
   * @var string
   * @Type("string")
   */
  private string $email;

  /**
   * Mobile number of the contact.
   *
   * @var string|null
   * @Type("string")
   */
  private ?string $mobileNr;

  /**
   * Language of the contact.
   *
   * @var string|null
   * @Type("string")
   */
  private ?string $lang;

  /**
   * Custom data associated with the contact.
   *
   * @var array
   * @Type("array<array<string,string>>")
   */
  private array $custom = [];

  /**
   * Get the ID of the contact.
   *
   * @return int
   *   Returns int.
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * Set the ID for the contact.
   *
   * @param int $id
   *   The ID to set.
   *
   * @return self
   *   Returns the instance of this class.
   */
  public function setId(int $id): self {
    $this->id = $id;
    return $this;
  }

  /**
   * Get the creation date.
   *
   * @return string
   *   Returns string.
   */
  public function getCreateDate(): string {
    return $this->createDate;
  }

  /**
   * Set the creation date.
   *
   * @param string $createDate
   *   The creation date to set.
   *
   * @return self
   *   Returns the instance of this class.
   */
  public function setCreateDate(string $createDate): self {
    $this->createDate = $createDate;
    return $this;
  }

  /**
   * Get the email address.
   *
   * @return string
   *   Returns string.
   */
  public function getEmail(): string {
    return $this->email;
  }

  /**
   * Set the email address.
   *
   * @param string $email
   *   The email address to set.
   *
   * @return self
   *   Returns the instance of this class.
   */
  public function setEmail(string $email): self {
    $this->email = $email;
    return $this;
  }

  /**
   * Get the mobile number.
   *
   * @return string
   *   Returns string or null.
   */
  public function getMobileNr(): string {
    return $this->mobileNr;
  }

  /**
   * Set the mobile number.
   *
   * @param string|null $mobileNr
   *   The mobile number to set.
   *
   * @return self
   *   Returns the instance of this class.
   */
  public function setMobileNr(?string $mobileNr): self {
    $this->mobileNr = $mobileNr;
    return $this;
  }

  /**
   * Get the language.
   *
   * @return string
   *   Returns string or null.
   */
  public function getLang(): string {
    return $this->lang;
  }

  /**
   * Set the language.
   *
   * @param string|null $lang
   *   The language to set.
   *
   * @return self
   *   Returns the instance of this class.
   */
  public function setLang(?string $lang): self {
    $this->lang = $lang;
    return $this;
  }

  /**
   * Get the custom data.
   *
   * @return array
   *   Returns array.
   */
  public function getCustom(): array {
    return $this->custom;
  }

  /**
   * Set the custom data.
   *
   * @param array $custom
   *   The custom data to set.
   *
   * @return self
   *   Returns the instance of this class.
   */
  public function setCustom(array $custom): self {
    $this->custom = $custom;
    return $this;
  }

}
