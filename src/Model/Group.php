<?php

namespace Drupal\webpower\Model;

// phpcs:ignore
use Symfony\Component\Validator\Constraints\Type;

/**
 * Contact model.
 */
class Group {

  /**
   * The unique identifier for the subscriber.
   *
   * @var int
   * @Type("int")
   */
  private $id;

  /**
   * Name of the subscriber.
   *
   * @var string
   * @Type("string")
   */
  private $name;

  /**
   * Indicates if the subscriber is for testing purposes.
   *
   * @var bool
   * @Type("bool")
   */
  private $isTest;

  /**
   * Indicates if the subscriber is active.
   *
   * @var bool
   * @Type("bool")
   */
  private $isActive;

  /**
   * Indicates if the subscriber is a system entity.
   *
   * @var bool
   * @Type("bool")
   */
  private $isSystem;

  /**
   * Remarks related to the subscriber.
   *
   * @var string
   * @Type("string")
   */
  private $remarks;

  /**
   * Gets the unique identifier for the subscriber.
   *
   * @return int
   *   Returns the ID.
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * Gets the name of the subscriber.
   *
   * @return string
   *   Returns the name.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Checks if the subscriber is for testing purposes.
   *
   * @return bool
   *   Returns true if for testing, otherwise false.
   */
  public function isTest(): bool {
    return $this->isTest;
  }

  /**
   * Checks if the subscriber is active.
   *
   * @return bool
   *   Returns true if active, otherwise false.
   */
  public function isActive(): bool {
    return $this->isActive;
  }

  /**
   * Checks if the subscriber is a system entity.
   *
   * @return bool
   *   Returns true if a system entity, otherwise false.
   */
  public function isSystem(): bool {
    return $this->isSystem;
  }

  /**
   * Gets remarks related to the subscriber.
   *
   * @return string
   *   Returns the remarks.
   */
  public function getRemarks(): string {
    return $this->remarks;
  }

  /**
   * Sets the unique identifier for the subscriber.
   *
   * @param int $id
   *   The subscriber ID.
   *
   * @return self
   *   Returns the Subscriber instance.
   */
  public function setId(int $id): self {
    $this->id = $id;
    return $this;
  }

  /**
   * Sets the name of the subscriber.
   *
   * @param string $name
   *   The subscriber name.
   *
   * @return self
   *   Returns the Subscriber instance.
   */
  public function setName(string $name): self {
    $this->name = $name;
    return $this;
  }

  /**
   * Sets if the subscriber is for testing purposes.
   *
   * @param bool $isTest
   *   True if for testing, otherwise false.
   *
   * @return self
   *   Returns the Subscriber instance.
   */
  public function setIsTest(bool $isTest): self {
    $this->isTest = $isTest;
    return $this;
  }

  /**
   * Sets if the subscriber is active.
   *
   * @param bool $isActive
   *   True if active, otherwise false.
   *
   * @return self
   *   Returns the Subscriber instance.
   */
  public function setIsActive(bool $isActive): self {
    $this->isActive = $isActive;
    return $this;
  }

  /**
   * Sets if the subscriber is a system entity.
   *
   * @param bool $isSystem
   *   True if a system entity, otherwise false.
   *
   * @return self
   *   Returns the Subscriber instance.
   */
  public function setIsSystem(bool $isSystem): self {
    $this->isSystem = $isSystem;
    return $this;
  }

  /**
   * Sets remarks related to the subscriber.
   *
   * @param string $remarks
   *   The remarks for the subscriber.
   *
   * @return self
   *   Returns the Subscriber instance.
   */
  public function setRemarks(string $remarks): self {
    $this->remarks = $remarks;
    return $this;
  }

}
