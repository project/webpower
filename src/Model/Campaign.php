<?php

namespace Drupal\webpower\Model;

// phpcs:ignore
use Symfony\Component\Validator\Constraints as Assert;
// phpcs:ignore
use Symfony\Component\Validator\Constraints\Type;

/**
 * The campaign model definition.
 */
class Campaign {

  /**
   * Campaign ID.
   *
   * @var int
   * @Type(int)
   */
  private int $id;

  /**
   * Brand ID.
   *
   * @var int
   * @Type(int)
   */
  private int $brandId;

  /**
   * Campaign name.
   *
   * @var string
   * @Type(string)
   */
  private string $name;

  /**
   * Campaign languages.
   *
   * @var arraystring
   * @Type(array<string>)
   * @Assert\All({
   *     @Assert\Type("string")
   * })
   */
  private array $lang;

  /**
   * Overall.
   *
   * @var bool
   * @Type(bool)
   */
  private bool $overall;

  /**
   * Deletable.
   *
   * @var bool
   * @Type(bool)
   */
  private bool $deletable;

  /**
   * Template only.
   *
   * @var bool
   * @Type(bool)
   */
  private bool $templateOnly;

  /**
   * From name.
   *
   * @var string
   * @Type(string)
   */
  private string $fromName;

  /**
   * Forward ID.
   *
   * @var int
   * @Type(int)
   */
  private int $forwardId;

  /**
   * Reply ID.
   *
   * @var int|null
   * @Type(int)
   */
  private ?int $replyId;

  /**
   * SMS from name.
   *
   * @var string|null
   * @Type(string)
   */
  private ?string $smsFromName;

  /**
   * Conversion points.
   *
   * @var array
   * @Type(array)
   * @Assert\All({
   *     @Assert\Type("mixed")
   * })
   */
  private array $conversionPoints;

  /**
   * Has SOAP API.
   *
   * @var bool
   * @Type(bool)
   */
  private bool $hasSoapApi;

  /**
   * Blacklists.
   *
   * @var arraystring
   * @Type(string)
   * @Assert\All({
   *     @Assert\Type("string")
   * })
   */
  private array $blacklists;

  /**
   * Local domain.
   *
   * @var array
   * @Type(array)
   * @Assert\All({
   *     @Assert\Type("mixed")
   * })
   */
  private array $localDomain;

  /**
   * Get Campaign ID.
   *
   * @return int
   *   Returns int.
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * Get Brand ID.
   *
   * @return int
   *   Returns int.
   */
  public function getBrandId(): int {
    return $this->brandId;
  }

  /**
   * Get campaign name.
   *
   * @return string
   *   Returns string.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Get campaign languages.
   *
   * @return arraystring
   *   Returns array.
   */
  public function getLang(): array {
    return $this->lang;
  }

  /**
   * Get overall value.
   *
   * @return bool
   *   Returns bool.
   */
  public function getOverall(): bool {
    return $this->overall;
  }

  /**
   * Get deletable value.
   *
   * @return bool
   *   Returns bool.
   */
  public function getDeletable(): bool {
    return $this->deletable;
  }

  /**
   * Get template only value.
   *
   * @return bool
   *   Returns bool.
   */
  public function getTemplateOnly(): bool {
    return $this->templateOnly;
  }

  /**
   * Get from name.
   *
   * @return string
   *   Returns string.
   */
  public function getFromName(): string {
    return $this->fromName;
  }

  /**
   * Get forward ID.
   *
   * @return int
   *   Returns int.
   */
  public function getForwardId(): int {
    return $this->forwardId;
  }

  /**
   * Get reply ID.
   *
   * @return int
   *   Returns int.
   */
  public function getReplyId(): int {
    return $this->replyId;
  }

  /**
   * Get SMS from name.
   *
   * @return string
   *   Returns string.
   */
  public function getSmsFromName(): string {
    return $this->smsFromName;
  }

  /**
   * Get conversion points.
   *
   * @return array
   *   Returns array.
   */
  public function getConversionPoints(): array {
    return $this->conversionPoints;
  }

  /**
   * Get if has SOAP API.
   *
   * @return bool
   *   Returns boolean.
   */
  public function getHasSoapApi(): bool {
    return $this->hasSoapApi;
  }

  /**
   * Get blacklists.
   *
   * @return arraystring
   *   Returns array.
   */
  public function getBlacklists(): array {
    return $this->blacklists;
  }

  /**
   * Get local domain.
   *
   * @return array
   *   Returns array.
   */
  public function getLocalDomain(): array {
    return $this->localDomain;
  }

  /**
   * Set Campaign ID.
   *
   * @param int $id
   *   Campaign ID.
   *
   * @return self
   *   Returns campaign ID.
   */
  public function setId(int $id): self {
    $this->id = $id;
    return $this;
  }

  /**
   * Set Brand ID.
   *
   * @param int $brandId
   *   Brand ID.
   *
   * @return self
   *   Returns brand ID.
   */
  public function setBrandId(int $brandId): self {
    $this->brandId = $brandId;
    return $this;
  }

  /**
   * Set campaign name.
   *
   * @param string $name
   *   Campaign name.
   *
   * @return self
   *   Returns campaign name.
   */
  public function setName(string $name): self {
    $this->name = $name;
    return $this;
  }

  /**
   * Set campaign languages.
   *
   * @param arraystring $lang
   *   Languages.
   *
   * @return self
   *   Returns languages.
   */
  public function setLang(array $lang): self {
    $this->lang = $lang;
    return $this;
  }

  /**
   * Set overall value.
   *
   * @param bool $overall
   *   Overall value.
   *
   * @return self
   *   Returns overall value.
   */
  public function setOverall(bool $overall): self {
    $this->overall = $overall;
    return $this;
  }

  /**
   * Set deletable value.
   *
   * @param bool $deletable
   *   Deleteable value.
   *
   * @return self
   *   Returns deletable value.
   */
  public function setDeletable(bool $deletable): self {
    $this->deletable = $deletable;
    return $this;
  }

  /**
   * Set template only value.
   *
   * @param bool $templateOnly
   *   TemplateOnly Value.
   *
   * @return self
   *   Returns TemplateOnly value.
   */
  public function setTemplateOnly(bool $templateOnly): self {
    $this->templateOnly = $templateOnly;
    return $this;
  }

  /**
   * Set from name.
   *
   * @param string $fromName
   *   FromName value.
   *
   * @return self
   *   Returns FromName value.
   */
  public function setFromName(string $fromName): self {
    $this->fromName = $fromName;
    return $this;
  }

  /**
   * Set forward ID.
   *
   * @param int $forwardId
   *   Forward ID value.
   *
   * @return self
   *   Returns Forward ID value.
   */
  public function setForwardId(int $forwardId): self {
    $this->forwardId = $forwardId;
    return $this;
  }

  /**
   * Set reply ID.
   *
   * @param int|null $replyId
   *   Reply ID value.
   *
   * @return self
   *   Returns Reply ID value.
   */
  public function setReplyId(?int $replyId): self {
    $this->replyId = $replyId;
    return $this;
  }

  /**
   * Set SMS from name.
   *
   * @param string|null $smsFromName
   *   SMS from name value.
   *
   * @return self
   *   Returns sms from name value.
   */
  public function setSmsFromName(?string $smsFromName): self {
    $this->smsFromName = $smsFromName;
    return $this;
  }

  /**
   * Set conversion points.
   *
   * @param array $conversionPoints
   *   Conversation points.
   *
   * @return self
   *   Returns Conversation points.
   */
  public function setConversionPoints(array $conversionPoints): self {
    $this->conversionPoints = $conversionPoints;
    return $this;
  }

  /**
   * Set if has SOAP API.
   *
   * @param bool $hasSoapApi
   *   Has soap API value.
   *
   * @return self
   *   Returns Soap API value.
   */
  public function setHasSoapApi(bool $hasSoapApi): self {
    $this->hasSoapApi = $hasSoapApi;
    return $this;
  }

  /**
   * Set blacklists.
   *
   * @param arraystring $blacklists
   *   Blacklists.
   *
   * @return self
   *   Returns blacklists.
   */
  public function setBlacklists(array $blacklists): self {
    $this->blacklists = $blacklists;
    return $this;
  }

  /**
   * Set local domain.
   *
   * @param array $localDomain
   *   Local domain.
   *
   * @return self
   *   Returns Local domain.
   */
  public function setLocalDomain(array $localDomain): self {
    $this->localDomain = $localDomain;
    return $this;
  }

}
