<?php

namespace Drupal\webpower\Model;

// phpcs:ignore
use Symfony\Component\Validator\Constraints\Type;

/**
 * Contact model.
 */
class Mailing {

  /**
   * The ID.
   *
   * @var int
   * @Type("int")
   */
  private int $id;

  /**
   * The name.
   *
   * @var string
   * @Type("string")
   */
  private string $name;

  /**
   * The language.
   *
   * @var string
   * @Type("string")
   */
  private string $lang;

  /**
   * The subject.
   *
   * @var string
   * @Type("string")
   */
  private string $subject;

  /**
   * The preheader.
   *
   * @var string
   * @Type("string")
   */
  private string $preheader;

  /**
   * The from name.
   *
   * @var string
   * @Type("string")
   */
  private string $fromName;

  /**
   * The forward ID.
   *
   * @var int|null
   * @Type("int")
   */
  private ?int $forwardId;

  /**
   * The reply ID.
   *
   * @var int|null
   * @Type("int")
   */
  private ?int $replyId;

  /**
   * The plaintext message.
   *
   * @var string
   * @Type("string")
   */
  private string $plaintextMsg;

  /**
   * The HTML message.
   *
   * @var string
   * @Type("string")
   */
  private string $htmlMsg;

  /**
   * Indicates if opt-in confirmation candidate.
   *
   * @var bool
   * @Type("bool")
   */
  private bool $optinConfirmCandidate;

  /**
   * Indicates if pictures are embedded.
   *
   * @var bool
   * @Type("bool")
   */
  private bool $embedPics;

  /**
   * Indicates if tracking pixel is embedded.
   *
   * @var bool
   * @Type("bool")
   */
  private bool $embedTrackpixel;

  /**
   * Transfer encoding.
   *
   * @var string
   * @Type("string")
   */
  private string $transferEncoding;

  /**
   * Transfer charset.
   *
   * @var string
   * @Type("string")
   */
  private string $transferCharset;

  /**
   * The last default sent date.
   *
   * @var \DateTime
   * @Type("\DateTime")
   */
  private \DateTime $lastDefSentDate;

  /**
   * Get ID.
   *
   * @return int
   *   Returns int.
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * Get the name.
   *
   * @return string
   *   Returns string.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Get the language.
   *
   * @return string
   *   Returns string.
   */
  public function getLang(): string {
    return $this->lang;
  }

  /**
   * Get the subject.
   *
   * @return string
   *   Returns string.
   */
  public function getSubject(): string {
    return $this->subject;
  }

  /**
   * Get the preheader.
   *
   * @return string
   *   Returns string.
   */
  public function getPreheader(): string {
    return $this->preheader;
  }

  /**
   * Get the from name.
   *
   * @return string
   *   Returns string.
   */
  public function getFromName(): string {
    return $this->fromName;
  }

  /**
   * Get the forward ID.
   *
   * @return int
   *   Returns int or null.
   */
  public function getForwardId(): int {
    return $this->forwardId;
  }

  /**
   * Get the reply ID.
   *
   * @return int
   *   Returns int or null.
   */
  public function getReplyId(): int {
    return $this->replyId;
  }

  /**
   * Get the plaintext message.
   *
   * @return string
   *   Returns string.
   */
  public function getPlaintextMsg(): string {
    return $this->plaintextMsg;
  }

  /**
   * Get the HTML message.
   *
   * @return string
   *   Returns string.
   */
  public function getHtmlMsg(): string {
    return $this->htmlMsg;
  }

  /**
   * Check if opt-in confirmation candidate.
   *
   * @return bool
   *   Returns bool.
   */
  public function isOptinConfirmCandidate(): bool {
    return $this->optinConfirmCandidate;
  }

  /**
   * Check if pictures are embedded.
   *
   * @return bool
   *   Returns bool.
   */
  public function isEmbedPics(): bool {
    return $this->embedPics;
  }

  /**
   * Check if tracking pixel is embedded.
   *
   * @return bool
   *   Returns bool.
   */
  public function isEmbedTrackpixel(): bool {
    return $this->embedTrackpixel;
  }

  /**
   * Get transfer encoding.
   *
   * @return string
   *   Returns string.
   */
  public function getTransferEncoding(): string {
    return $this->transferEncoding;
  }

  /**
   * Get transfer charset.
   *
   * @return string
   *   Returns string.
   */
  public function getTransferCharset(): string {
    return $this->transferCharset;
  }

  /**
   * Get the last default sent date.
   *
   * @return \DateTime
   *   Returns DateTime or null.
   */
  public function getLastDefSentDate(): \DateTime {
    return $this->lastDefSentDate;
  }

  /**
   * Set the ID.
   *
   * @param int $id
   *   The ID.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setId(int $id): self {
    $this->id = $id;
    return $this;
  }

  /**
   * Set the name.
   *
   * @param string $name
   *   The name.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setName(string $name): self {
    $this->name = $name;
    return $this;
  }

  /**
   * Set the language.
   *
   * @param string $lang
   *   The language.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setLang(string $lang): self {
    $this->lang = $lang;
    return $this;
  }

  /**
   * Set the subject.
   *
   * @param string $subject
   *   The subject.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setSubject(string $subject): self {
    $this->subject = $subject;
    return $this;
  }

  /**
   * Set the preheader.
   *
   * @param string $preheader
   *   The preheader.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setPreheader(string $preheader): self {
    $this->preheader = $preheader;
    return $this;
  }

  /**
   * Set the from name.
   *
   * @param string $fromName
   *   The from name.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setFromName(string $fromName): self {
    $this->fromName = $fromName;
    return $this;
  }

  /**
   * Set the forward ID.
   *
   * @param int|null $forwardId
   *   The forward ID.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setForwardId(?int $forwardId): self {
    $this->forwardId = $forwardId;
    return $this;
  }

  /**
   * Set the reply ID.
   *
   * @param int|null $replyId
   *   The reply ID.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setReplyId(?int $replyId): self {
    $this->replyId = $replyId;
    return $this;
  }

  /**
   * Set the plaintext message.
   *
   * @param string $plaintextMsg
   *   The plaintext message.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setPlaintextMsg(string $plaintextMsg): self {
    $this->plaintextMsg = $plaintextMsg;
    return $this;
  }

  /**
   * Set the HTML message.
   *
   * @param string $htmlMsg
   *   The HTML message.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setHtmlMsg(string $htmlMsg): self {
    $this->htmlMsg = $htmlMsg;
    return $this;
  }

  /**
   * Set opt-in confirmation candidate.
   *
   * @param bool $optinConfirmCandidate
   *   Opt-in confirmation candidate status.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setOptinConfirmCandidate(bool $optinConfirmCandidate): self {
    $this->optinConfirmCandidate = $optinConfirmCandidate;
    return $this;
  }

  /**
   * Set embed pictures status.
   *
   * @param bool $embedPics
   *   Embed pictures status.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setEmbedPics(bool $embedPics): self {
    $this->embedPics = $embedPics;
    return $this;
  }

  /**
   * Set embed tracking pixel status.
   *
   * @param bool $embedTrackpixel
   *   Embed tracking pixel status.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setEmbedTrackpixel(bool $embedTrackpixel): self {
    $this->embedTrackpixel = $embedTrackpixel;
    return $this;
  }

  /**
   * Set the transfer encoding.
   *
   * @param string $transferEncoding
   *   The transfer encoding.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setTransferEncoding(string $transferEncoding): self {
    $this->transferEncoding = $transferEncoding;
    return $this;
  }

  /**
   * Set the transfer charset.
   *
   * @param string $transferCharset
   *   The transfer charset.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setTransferCharset(string $transferCharset): self {
    $this->transferCharset = $transferCharset;
    return $this;
  }

  /**
   * Set the last default sent date.
   *
   * @param \DateTime $lastDefSentDate
   *   The last default sent date.
   *
   * @return self
   *   Returns an instance of the class.
   */
  public function setLastDefSentDate(\DateTime $lastDefSentDate): self {
    $this->lastDefSentDate = $lastDefSentDate;
    return $this;
  }

}
