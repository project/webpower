<?php

namespace Drupal\webpower\Model;

// phpcs:ignore
use Symfony\Component\Validator\Constraints\Type;

/**
 * The Field model definition.
 */
class Field {

  /**
   * The name of the field.
   *
   * @var string
   * @Type("string")
   */
  private string $name;

  /**
   * An array of descriptions keyed by language.
   *
   * @var arraystring
   * @Type("array<string>")
   */
  private array $description = [];

  /**
   * The type of the field.
   *
   * @var string
   * @Type("string")
   */
  private string $type;

  /**
   * The length of the field.
   *
   * @var int
   * @Type("int")
   */
  private int $length;

  /**
   * The default value of the field.
   *
   * @var string|null
   * @Type("string")
   */
  private ?string $default = NULL;

  /**
   * Whether the field is required.
   *
   * @var bool
   * @Type("bool")
   */
  private bool $required;

  /**
   * Get the name.
   *
   * @return string
   *   Returns string.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Get the description.
   *
   * @return array
   *   Returns array.
   */
  public function getDescription(): array {
    return $this->description;
  }

  /**
   * Get the type.
   *
   * @return string
   *   Returns string.
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * Get the length.
   *
   * @return int
   *   Returns int.
   */
  public function getLength(): int {
    return $this->length;
  }

  /**
   * Get the default value.
   *
   * @return string|null
   *   Returns string or null.
   */
  public function getDefault(): ?string {
    return $this->default;
  }

  /**
   * Check if required.
   *
   * @return bool
   *   Returns bool.
   */
  public function isRequired(): bool {
    return $this->required;
  }

  /**
   * Set the name.
   *
   * @param string $name
   *   The name of the field.
   *
   * @return self
   *   Returns the Field object.
   */
  public function setName(string $name): self {
    $this->name = $name;
    return $this;
  }

  /**
   * Set the description.
   *
   * @param array $description
   *   An array of descriptions keyed by language.
   *
   * @return self
   *   Returns the Field object.
   */
  public function setDescription(array $description): self {
    $this->description = $description;
    return $this;
  }

  /**
   * Set the type.
   *
   * @param string $type
   *   The type of the field.
   *
   * @return self
   *   Returns the Field object.
   */
  public function setType(string $type): self {
    $this->type = $type;
    return $this;
  }

  /**
   * Set the length.
   *
   * @param int $length
   *   The length of the field.
   *
   * @return self
   *   Returns the Field object.
   */
  public function setLength(int $length): self {
    $this->length = $length;
    return $this;
  }

  /**
   * Set the default value.
   *
   * @param string|null $default
   *   The default value of the field.
   *
   * @return self
   *   Returns the Field object.
   */
  public function setDefault(?string $default): self {
    $this->default = $default;
    return $this;
  }

  /**
   * Set if required.
   *
   * @param bool $required
   *   Whether the field is required.
   *
   * @return self
   *   Returns the Field object.
   */
  public function setRequired(bool $required): self {
    $this->required = $required;
    return $this;
  }

}
