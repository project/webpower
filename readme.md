
The Webpower module provides a bridge between Drupal and the [Spotler Webpower](https://spotler.nl/webpower) mailing application. It allows you to define blocks for mailing campagins created in Webpower through Webpower's API.

https://www.drupal.org/project/webpower

# Features
- Client for the Webpower API
- Fetch Webpower Mailing campaigns to be used in blocks
- Map campaign fields to standard drupal fields
- Generates forms in blocks for selected campaigns
- Submit / Update mailing subscriptions

# Post-Installation
- Go to the Webpower settings form under Config -> Webservices
- Input your Webpower API access data (Client ID, Client Secret & License)
- In Block Overview, create a new Webpower Newsletter Form Block
- Select the campaign required for your website, and map campaign fields to Drupal fields

# Dependencies
- The module requires no additional modules or libraries to be installed.